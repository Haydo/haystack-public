//to be populated by whoever
let onEnter = function() {};
let onEscape = function() {};
let onDelete = function() {};

let Events = {
    init: function() {
        //onclicks
        document.querySelector("#window-ctrl-close-btn").onclick = function () {
            RendererDispatch.closeApp();
        }
    
        document.querySelector("#window-ctrl-rest-max-btn").onclick = function () {
            RendererDispatch.maximizeApp();
        }
    
        document.querySelector("#window-ctrl-minimize-btn").onclick = function () {
            RendererDispatch.minimizeApp();
        }
    
        document.querySelector('#saved-connections-collapse-btn').onclick = function() {
            this.classList.toggle('collapsed');
            document.querySelector('#saved-connections-list').style.display = this.classList.contains('collapsed') ? "none" : "block";
        }
    
        document.querySelector('#saved-auth-profiles-collapse-btn').onclick = function() {
            this.classList.toggle('collapsed');
            document.querySelector('#saved-auth-profiles-list').style.display = this.classList.contains('collapsed') ? "none" : "block";
        }

        document.querySelector('#saved-search-rules-collapse-btn').onclick = function() {
            this.classList.toggle('collapsed');
            document.querySelector('#saved-search-rules-list').style.display = this.classList.contains('collapsed') ? "none" : "block";
        }
    
        document.querySelector('#search-btn').onclick = function() {
            let searchValue = document.querySelector('#search-input').value;
            let searchKey = document.querySelector('#search-dropdown').value;
    
            if(isFilter) {
                queryByFilter(state.activeTabs.connection.textContent, searchValue);
            } else if (searchRules.hasOwnProperty(searchKey)){
                queryBySearchRule(state.activeTabs.connection.textContent, searchKey, searchValue);
            } else {
                queryByAttrs(state.activeTabs.connection.textContent, searchKey, searchValue);
            }
        }
    
        document.querySelector('#add-connection-btn').onclick = function() {
            editConnectionParameters({
                name: "",
                host: "",
                port: "",
                useTLS: true,
                authProfile: "",
                username: "",
                password: ""
            });
        }
    
        document.querySelector('#add-auth-profiles-btn').onclick = function() {
            onEnter = function() {
                document.querySelector('#auth-profile-prompt-next-btn').click();
                onEnter = function(){};
            }
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#auth-profile-prompt-container').classList.toggle('hide');
        }

        document.querySelector('#add-search-rules-btn').onclick = function() {
            onEnter = function() {
                document.querySelector('#search-rule-prompt-next-btn').click();
                onEnter = function(){};
            }
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#search-rule-prompt-container').classList.toggle('hide');
        }
    
        document.querySelector('#search-input').onkeydown = function(e) {
            if(e.key == "(") {
                let caretPos = this.selectionStart;
                this.value = this.value.slice(0, caretPos) + ")" + this.value.slice(caretPos);
                this.setSelectionRange(caretPos, caretPos);
            }
        }
    
        document.querySelector('#search-input').onkeyup = function(e) {
            let inputVal = this.value;
            let openParenthCount = (inputVal.match(new RegExp('[(]', 'g')) || []).length;
            let closedParenthCount = (inputVal.match(new RegExp('[)]', 'g')) || []).length;
    
            if((openParenthCount > 0 || closedParenthCount > 0) && !isFilter) {
                isFilter = true;
                document.querySelector('#search-dropdown').classList.toggle('locked');
                document.querySelector('#search-dropdown').disabled = true;
            } else if((openParenthCount <= 0 && closedParenthCount <= 0) && isFilter){
                isFilter = false;
                document.querySelector('#search-dropdown').classList.toggle('locked');
                document.querySelector('#search-dropdown').disabled = false;
            }
    
            if(openParenthCount !== closedParenthCount) {
                this.style.color = "rgb(255, 80, 80)";
            } else {
                this.style.color = "inherit";
            }
        }

        //focus/blur
        document.querySelector('#search-input').addEventListener('focus', () => {
            onEnter = function() {
                document.querySelector('#search-btn').click();
            }
        });
        document.querySelector('#search-input').addEventListener('blur', () => {
            onEnter = function() {};
        });
    
        //keypresses
        document.addEventListener('keyup', (e) => {
            //enter
            if(e.keyCode === 13) {
                e.preventDefault();
                onEnter();
            }

            //escape
            if(e.keyCode === 27) {
                e.preventDefault();
                onEscape();
            }

            //delete
            if(e.keyCode === 46) {
                e.preventDefault();
                onDelete();
            }
        });
    }
}