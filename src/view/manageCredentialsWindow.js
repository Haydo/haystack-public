let ManageCredentialsWindow = {
    init: function() {
        document.querySelector('#add-credentials-header-btn').onclick = function() {
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#add-credentials-container').classList.toggle('hide');
        }

        document.querySelector('#add-credentials-next-btn').onclick = function() {
            let validToSave = true;
            //save connection params
            saveConnectionParams();

            //connect to ldap host by passing in connection name
            let connectionName = document.querySelector("#add-connection-connection-name").value;
            if(document.querySelector("#use-profile-yes").checked) {

            } else {
                connectToHost(connectionName, document.querySelector('#add-credentials-password').value);
            }

            document.querySelector('#add-credentials-container').classList.toggle('hide');
            document.querySelector('#overlay').classList.toggle('hide');
        }

        document.querySelector("#add-credentials-profile-radio-container").onclick = function() {
            if(document.querySelector("#use-profile-yes").checked) {
                //check to see if profile containers are hidden and unhide
                if(document.querySelector(".use-profile-container").classList.contains('hide')) {
                    //unhide all profile containers
                    let profileContainers = document.querySelectorAll(".use-profile-container");
                    for(let i = 0; i < profileContainers.length; i++) {
                        profileContainers[i].classList.toggle('hide');
                    }

                    //hide all non-profile containser
                    let nonProfileContainers = document.querySelectorAll(".no-profile-container");
                    for(let i = 0; i < nonProfileContainers.length; i++) {
                        nonProfileContainers[i].classList.toggle('hide');
                    }
                }
            } else {
                //check to see if profile containers are hidden and hide
                if(document.querySelector(".no-profile-container").classList.contains('hide')) {
                    //hide all profile containers
                    let profileContainers = document.querySelectorAll(".use-profile-container");
                    for(let i = 0; i < profileContainers.length; i++) {
                        profileContainers[i].classList.toggle('hide');
                    }

                    //unhide all non-profile containser
                    let nonProfileContainers = document.querySelectorAll(".no-profile-container");
                    for(let i = 0; i < nonProfileContainers.length; i++) {
                        nonProfileContainers[i].classList.toggle('hide');
                    }
                }
            }
        }
    }
}


editCredentialParameters = function(connectionObject) {
    if(typeof(connectionObject) !== "undefined") {
        document.querySelector('#profile-dropdown').value = (typeof(connectionObject.name) !== "undefined") ? connectionObject.authProfile : "";
        document.querySelector('#add-credentials-username').value = (typeof(connectionObject.host) !== "undefined") ? connectionObject.username : "";

        let toCheck = (typeof(connectionObject.usesAuthProfile) === "undefined" || connectionObject.usesAuthProfile) ? "#use-profile-yes" : "#use-profile-no";
        document.querySelector(toCheck).click();
        
        onEnter = function() {
            document.querySelector('#add-credentials-next-btn').click();
            onEnter = function(){};
        }
    
        document.querySelector('#add-credentials-container').classList.toggle('hide');
    } else {
        console.log("passed in an undefined connection object");
    }
}