const electron = require('electron');

const{app, ipcRenderer} = electron;

let RendererDispatch = {


    closeApp() {
        ipcRenderer.send('ldap:client:unbindAll');
        ipcRenderer.send('window:close');
    },
    maximizeApp() {
        ipcRenderer.send('window:maximize');
    },
    minimizeApp() {
        ipcRenderer.send('window:minimize');
    },
    saveConnectionToConfig(connection) {
        ipcRenderer.send('config:connection:modify', connection);
    },
    deleteConnectionFromConfig(connectionName) {
        ipcRenderer.send('config:connection:delete', connectionName);
    },
    saveProfileToConfig(profile) {
        ipcRenderer.send('config:profile:modify', profile);
    },
    deleteProfileFromConfig(profileName) {
        ipcRenderer.send('config:profile:delete', profileName);
    },
    saveSearchRuleToConfig(rule) {
        ipcRenderer.send('config:searchrule:modify', rule);
    },
    deleteSearchRuleFromConfig(ruleName) {
        ipcRenderer.send('config:searchrule:delete', ruleName);
    },
    connectWithProfile(connection) {
        ipcRenderer.send('ldap:client:connect:profile', connection);
    },
    connectWithBasicAuth(connection) {
        ipcRenderer.send('ldap:client:connect:basic', connection);
        connection = null;
    },
    createAuthProfile(profile) {
        ipcRenderer.send('auth:keytar:profile:store', profile);
        profile = null;
    },
    deleteAuthProfile(profilename) {
        ipcRenderer.send('auth:keytar:profile:delete', profilename);
    },
    getFullResultSet(data) {
        ipcRenderer.send('ldap:client:get:record', data);
    },
    replaceAttributeValue(modification) {
        ipcRenderer.send('ldap:client:attr:replace', modification);
    },
    closeConnection(connectionName) {
        ipcRenderer.send('ldap:client:unbind', connectionName);
    }
}

ipcRenderer.on('load:connection-list', (event, message) => {
    initConnectionList(message);
});

ipcRenderer.on('load:search-rules-list', (event, message) => {
    initSearchRules(message);     
});

ipcRenderer.on('load:auth-profiles-list', (event, message) => {
    initAuthProfiles(message);
});

ipcRenderer.on('ldap:client:query:results', (event, message) => {
    clearQueryResults();
    populateQueryResults(message);
});

ipcRenderer.on('ldap:client:object:results', (event, message) => {
    createObjectTab(message);
});

ipcRenderer.on('ldap:client:connection:success', (event, message) => {
    createConnectionTab(message);
})
