const template = [
    {
        label: 'File',
        submenu: [
            {
                label: "Test 1"
            },
            {
                label: "Test 2"
            }
        ]
    }
]

// check if user is running MacOS and add empty object accordingly
if (process.platform === 'darwin') {
    template.unshift({});
}

exports.template = template;