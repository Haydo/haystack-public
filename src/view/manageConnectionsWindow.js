let ManageConnectionWindow = {
    init: function() {
        document.querySelector('#add-connection-header-btn').onclick = function() {
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#add-connection-container').classList.toggle('hide');
        }

        document.querySelector('#add-connection-next-btn').onclick = function() {
            let validParams = true;
            let params = {};

            params.name = document.querySelector('#add-connection-connection-name').value;
            params.host = document.querySelector('#add-connection-hostname').value;
            params.port = document.querySelector('#add-connection-port').value;
            params.useTLS = document.querySelector('#use-ldap-yes').checked;

            for(var i in params) {
                if(params[i].length == 0) {
                    validParams = false;
                    console.log("Bad params. Warn user");
                    break;
                }
            }

            if(validParams) {
                document.querySelector('#add-connection-container').classList.toggle('hide');

                editCredentialParameters(ManageConnectionWindow.connectionObject);
            }
        }
    },
    connectionObject: {}
}

editConnectionParameters = function(connectionObject) {
    if(typeof(connectionObject) !== "undefined") {
        ManageConnectionWindow.connectionObject = connectionObject;

        document.querySelector('#add-connection-connection-name').value = (typeof(connectionObject.name) !== "undefined") ? connectionObject.name : "";
        document.querySelector('#add-connection-hostname').value = (typeof(connectionObject.host) !== "undefined") ? connectionObject.host : "";
        document.querySelector('#add-connection-port').value = (typeof(connectionObject.port) !== "undefined") ? connectionObject.port : "";

        let toCheck = (typeof(connectionObject.useTLS) === "undefined" || connectionObject.useTLS) ? "#use-ldap-yes" : "#use-ldap-no";
        document.querySelector(toCheck).click();

        onEnter = function() {
            document.querySelector('#add-connection-next-btn').click();
        }
    
        document.querySelector('#overlay').classList.toggle('hide');
        document.querySelector('#add-connection-container').classList.toggle('hide');
    } else {
        console.log("passed in an undefined connection object");
    }
}