function createConnectionObject(connectionObject) {
    let parent = document.querySelector("#saved-connections-list");
    let newConnectionElement = document.createElement('div');
    newConnectionElement.className = "config-object";
    
    /*
    let connectionImage = document.createElement('div');
    connectionImage.className = "connection-object-image";
    newConnectionElement.appendChild(connectionImage);
*/
    let connectionName = document.createElement('div');
    connectionName.className = "config-object-name";
    connectionName.innerHTML = connectionObject.name;
    connectionName.onclick = function() {
        let connectionTabs = document.querySelectorAll('.connection.tab');
        let match = false;
        for(let i = 0; i < connectionTabs.length; i++) {
            if(connectionTabs[i].textContent === connectionObject.name) {
                match = true;
                break;
            }
        }

        if(!match) {
            connectToHost(connectionObject.name);
        }
    }
    newConnectionElement.appendChild(connectionName);

    let connectionButtonContainer = document.createElement('div');
    connectionButtonContainer.className = "config-object-btn-container";
    connectionButtonContainer.style.display = "none";
    newConnectionElement.appendChild(connectionButtonContainer);

    let connectionEditBtn = document.createElement('button');
    connectionEditBtn.className = "config-object-btn edit-btn";
    connectionButtonContainer.appendChild(connectionEditBtn);
    connectionEditBtn.onclick = function() {
        editConnectionParameters(connectionObject);
    }

    let connectionDeleteBtn = document.createElement('button');
    connectionDeleteBtn.className = "config-object-btn delete-btn";
    connectionButtonContainer.appendChild(connectionDeleteBtn);
    connectionDeleteBtn.onclick = function() {
        deleteConnection(connectionObject.name);
    }

    parent.appendChild(newConnectionElement);

    newConnectionElement.onmouseover = function() {
        connectionButtonContainer.style.display = "block";
    }
   
    newConnectionElement.onmouseout = function() {
        connectionButtonContainer.style.display = "none";
    }
}

function createConnectionTab(connectionName) {
    //create tab container
    //first check to see if the tab bar has been created (not true if no tabs yet open)
    if(document.querySelector('#connection-tab-container') === null) {
        let tabContainer = document.createElement('div');
        tabContainer.id = "connection-tab-container";
        tabContainer.className = "tab-container";

        let workspace = document.querySelector('#workspace-container');
        workspace.insertBefore(tabContainer, workspace.childNodes[0]);
    }


    //create tab
    let tab;

    //then check if the tab requested already exists
    let tabExists = false;
    let tabs = document.querySelectorAll('.connection.tab');

    for(var i = 0; i < tabs.length; i++) {
        if(tabs[i].textContent == connectionName) {
            tabExists = true;
            //TODO Switch to tab
            //selectTab(tabs[i], 'connection');
            tab = tabs[i];
            break;
        }
    }


    if(!tabExists) {
        let parent = document.querySelector("#connection-tab-container");

        let newTabElement = document.createElement('div');
        newTabElement.className = "connection tab";

        newTabElement.onclick = function(e) {
            if(!e.target.className.includes('delete')) {
                if(state.activeResults) {
                    state.activeResults.classList.toggle('hide');
                }
    
                selectConnectionTab(newTabElement);

                let containerToUnhide = document.querySelector('.results-container.' + state.activeTabs.connection.textContent);
                state.activeResults = containerToUnhide;
                if(state.activeResults.classList.contains('hide')) {
                    state.activeResults.classList.toggle('hide');
                }
            }

        }

        let tabName = document.createElement('div');
        tabName.className = "tab-name";
        tabName.innerHTML = connectionName;
        newTabElement.appendChild(tabName);

        let tabCloseBtn = document.createElement('button');
        tabCloseBtn.className = "tab-delete-btn";
        newTabElement.appendChild(tabCloseBtn);
        tabCloseBtn.onclick = function() {
            let containerToDelete = document.querySelector('.results-container.' + connectionName);
            containerToDelete.parentNode.removeChild(containerToDelete);

            removeConnectionTab(newTabElement);
            closeConnection(connectionName);
        }

        parent.appendChild(newTabElement);
        //selectTab(newTabElement, 'connection');
        tab = newTabElement;
    }


    //create results shell
    let parent = document.querySelector('#workspace-container');
    
    let resultsContainer = document.createElement('div');
    resultsContainer.className = 'results-container ' + connectionName;

    let queryResultsContainer = document.createElement('div');
    queryResultsContainer.className = 'query-results-container ' + connectionName;

    let objectInfoContainer = document.createElement('div');
    objectInfoContainer.className = 'object-info-container ' + connectionName;

    let objectTabContainer = document.createElement('div');
    objectTabContainer.className = 'object-tab-container tab-container ' + connectionName;

    let objectTabRefreshButton = document.createElement('button');
    objectTabRefreshButton.className = 'refresh-btn ' + connectionName;

    objectTabContainer.appendChild(objectTabRefreshButton);

    objectInfoContainer.appendChild(objectTabContainer);
    resultsContainer.appendChild(queryResultsContainer);
    resultsContainer.appendChild(objectInfoContainer);
    parent.appendChild(resultsContainer);


    tab.click();
}

function createObjectTab(record) {
    console.log(record);
    //Create Tab
    let tab;

    //test that the tab does not already exist
    let tabExists = false;
    let currentConnection = state.activeTabs.connection.textContent;
    let tabs = document.querySelectorAll('.record.tab.' + currentConnection);

    for(var i = 0; i < tabs.length; i++) {
        if(tabs[i].textContent === record.dn) {
            tabExists = true;
            //TODO Switch to tab
            //selectTab(tabs[i], 'connection');
            tab = tabs[i];
            tab.click();
            break;
        }
    }
    
    if(!tabExists) {
        if(document.querySelector('.object-info-container.' + currentConnection).classList.value.includes('hide')) {
            document.querySelector('.object-info-container.' + currentConnection).classList.toggle('hide');
        }

        let parent = document.querySelector('.object-tab-container.' + currentConnection);
        let newTabElement = document.createElement('div');
        newTabElement.className = 'record tab ' + currentConnection;
        newTabElement.onclick = function(e) {
            //check the user didn't click on the x button
            if(!e.target.className.includes('delete')) {
                if(typeof(state.activeRecord[currentConnection]) !== 'undefined') {
                    state.activeRecord[currentConnection].classList.toggle('hide');
                }

                selectRecordTab(newTabElement, currentConnection);
                let currentRecord = state.activeTabs.record[currentConnection].textContent.replace(/[= ]|,/g, '-');

                let refreshButton = document.querySelector('.refresh-btn.' + currentConnection);
                refreshButton.onclick = function() {
                    getFullResultSet(state.activeTabs.record[currentConnection].textContent)
                };
                

                let containerToUnhide = document.querySelector('.object-info-wrapper.' + currentRecord);
                state.activeRecord[currentConnection] = containerToUnhide;    
                if(state.activeRecord[currentConnection].classList.contains('hide')) {
                    state.activeRecord[currentConnection].classList.toggle('hide');
                }
            }
        }

        let tabName = document.createElement('div');
        tabName.className = "tab-name";
        tabName.innerHTML = record.dn;
        newTabElement.appendChild(tabName);

        let tabCloseBtn = document.createElement('button');
        tabCloseBtn.className = "tab-delete-btn";
        newTabElement.appendChild(tabCloseBtn);
        tabCloseBtn.onclick = function() {
            let containerToDelete = document.querySelector('.object-info-wrapper.' + record.dn.replace(/[= ]|,/g, '-'));
            containerToDelete.parentNode.removeChild(containerToDelete);
            removeRecordTab(newTabElement, currentConnection);
            //closeConnection(connectionObject);
        }

        parent.appendChild(newTabElement);

        //Create Result Sheet
        parent = document.querySelector('.object-info-container.' + currentConnection);

        let objectInfoWrapper = document.createElement('div');
        objectInfoWrapper.className = 'object-info-wrapper ' + record.dn.replace(/[= ]|,/g, '-');

        let objectInfo = document.createElement('div');
        objectInfo.className = 'object-info';

        objectInfoWrapper.appendChild(objectInfo);
        parent.appendChild(objectInfoWrapper);

        newTabElement.click();
    }

    let objectInfo = state.activeRecord[currentConnection].querySelector('.object-info');
    if(objectInfo !== null) {
        objectInfo.innerHTML = '';
    }


    for(var i in record) {
        if(typeof(record[i]) === 'object') {
            for(let j = 0; j < record[i].length; j++) {
                let attributeContainer = document.createElement('div');
                attributeContainer.className = 'attribute-container';
                attributeContainer.onclick = function() {
                    if(typeof(state.selectedAttribute) !== 'undefined') {
                        state.selectedAttribute.element.classList.toggle('selected');
                    }
    
                    attributeContainer.classList.toggle('selected');
                    state.selectedAttribute = {
                        connection: currentConnection,
                        record: state.activeTabs.record[currentConnection].textContent,
                        attribute: attributeContainer.children[0].textContent,
                        value: attributeContainer.children[1].value,
                        element: attributeContainer
                    }
                }
        
                let attributeKey = document.createElement('div');
                attributeKey.className = 'attribute-key';
                attributeKey.innerHTML = i;
        
                let attributeValue = document.createElement('input');
                attributeValue.className = 'attribute-value disabled';
                attributeValue.toggleAttribute('readonly');

                attributeContainer.classList.toggle('collapsable');
                attributeValue.value = (record[i][j]);

                attributeContainer.appendChild(attributeKey);
                attributeContainer.appendChild(attributeValue);
                objectInfo.appendChild(attributeContainer);

                attributeValue.addEventListener('dblclick', () => {
                    if(attributeValue.classList.contains('disabled')) {
        
                        state.currentVal = attributeValue.value;
                        attributeValue.classList.toggle('disabled');
                        attributeValue.toggleAttribute('readonly');
                        attributeValue.select();
                    }
                });
        
                attributeValue.addEventListener('blur', () => {
                    if(!attributeValue.classList.contains('disabled')) {
                        state.newVal = attributeValue.value;
                        attributeValue.classList.toggle('disabled');
                        attributeValue.toggleAttribute('readonly');
    
                        if(state.newVal !== state.currentVal) {
                            RendererDispatch.replaceAttributeValue({
                                connection: state.selectedAttribute.connection,
                                dn: state.selectedAttribute.record,
                                oldValue: state.currentVal,
                                newValue: state.newVal,
                                attribute: state.selectedAttribute.attribute
                            })
                        }
                    }
                });
            }
        } else {
            let attributeContainer = document.createElement('div');
            attributeContainer.className = 'attribute-container';
            attributeContainer.onclick = function() {
                if(typeof(state.selectedAttribute) !== 'undefined') {
                    state.selectedAttribute.element.classList.toggle('selected');
                }

                attributeContainer.classList.toggle('selected');
                state.selectedAttribute = {
                    connection: currentConnection,
                    record: state.activeTabs.record[currentConnection].textContent,
                    attribute: attributeContainer.children[0].textContent,
                    value: attributeContainer.children[1].value,
                    element: attributeContainer
                }
            }
    
            let attributeKey = document.createElement('div');
            attributeKey.className = 'attribute-key';
            attributeKey.innerHTML = i;
    
            let attributeValue = document.createElement('input');
            attributeValue.className = 'attribute-value disabled';
            attributeValue.toggleAttribute('readonly');

            attributeValue.value = (record[i]);

            attributeContainer.appendChild(attributeKey);
            attributeContainer.appendChild(attributeValue);
            objectInfo.appendChild(attributeContainer);

            attributeValue.addEventListener('dblclick', () => {
                if(attributeValue.classList.contains('disabled')) {
    
                    state.currentVal = attributeValue.value;
                    attributeValue.classList.toggle('disabled');
                    attributeValue.toggleAttribute('readonly');
                    attributeValue.select();
                }
            });
    
            attributeValue.addEventListener('blur', () => {
                if(!attributeValue.classList.contains('disabled')) {
                    state.newVal = attributeValue.value;
                    attributeValue.classList.toggle('disabled');
                    attributeValue.toggleAttribute('readonly');

                    if(state.newVal !== state.currentVal) {
                        RendererDispatch.replaceAttributeValue({
                            connection: state.selectedAttribute.connection,
                            dn: state.selectedAttribute.record,
                            oldValue: state.currentVal,
                            newValue: state.newVal,
                            attribute: state.selectedAttribute.attribute
                        })
                    }
                }
            });
        }
    }

    if(typeof(state.selectedAttribute) !== 'undefined') {
        state.selectedAttribute.element.click();
    }
}

function createAuthProfileObject(authProfileOjbect) {
    let parent = document.querySelector("#saved-auth-profiles-list");
    let newAuthProfileElement = document.createElement('div');
    newAuthProfileElement.className = "config-object";

    let profileName = document.createElement('div');
    profileName.className = "config-object-name";
    profileName.innerHTML = authProfileOjbect.name;

    newAuthProfileElement.appendChild(profileName);

    let buttonContainer = document.createElement('div');
    buttonContainer.className = "config-object-btn-container";
    buttonContainer.style.display = "none";
    newAuthProfileElement.appendChild(buttonContainer);

    let editBtn = document.createElement('button');
    editBtn.className = "config-object-btn edit-btn";
    editBtn.onclick = function() {
        editAuthProfileParameters(authProfileOjbect);
    }

    buttonContainer.appendChild(editBtn);

    let deleteBtn = document.createElement('button');
    deleteBtn.className = "config-object-btn delete-btn";
    deleteBtn.onclick = function() {
        deleteAuthProfile(authProfileOjbect.name);
    }

    buttonContainer.appendChild(deleteBtn);

    parent.appendChild(newAuthProfileElement);

    newAuthProfileElement.onmouseover = function() {
        buttonContainer.style.display = "block";
    }
   
    newAuthProfileElement.onmouseout = function() {
        buttonContainer.style.display = "none";
    }
}

function createSearchRuleObject(searchRuleObject) {
    let parent = document.querySelector("#saved-search-rules-list");
    let newSearchRuleElement = document.createElement('div');
    newSearchRuleElement.className = "config-object";

    let ruleName = document.createElement('div');
    ruleName.className = "config-object-name";
    ruleName.innerHTML = searchRuleObject.name;

    newSearchRuleElement.appendChild(ruleName);

    let buttonContainer = document.createElement('div');
    buttonContainer.className = "config-object-btn-container";
    buttonContainer.style.display = "none";
    newSearchRuleElement.appendChild(buttonContainer);

    let editBtn = document.createElement('button');
    editBtn.className = "config-object-btn edit-btn";
    editBtn.onclick = function() {
        editSearchRuleParameters(searchRuleObject);
    }

    buttonContainer.appendChild(editBtn);

    let deleteBtn = document.createElement('button');
    deleteBtn.className = "config-object-btn delete-btn";
    deleteBtn.onclick = function() {
        deleteSearchRule(searchRuleObject.name);
    }

    buttonContainer.appendChild(deleteBtn);

    parent.appendChild(newSearchRuleElement);

    newSearchRuleElement.onmouseover = function() {
        buttonContainer.style.display = "block";
    }
   
    newSearchRuleElement.onmouseout = function() {
        buttonContainer.style.display = "none";
    }
}


function createResultsCount(count) {
    let parent = document.querySelector('.query-results-container.' + state.activeTabs.connection.textContent);
    let countContainer = document.createElement('div');
    countContainer.className = 'result-count-container';
    let suffix = (count === 1) ? ' entry:' : ' entries:';
    countContainer.textContent = count + suffix;

    parent.appendChild(countContainer);
}

function createResultRecord(record, isSingle) {
    let parent = document.querySelector('.query-results-container.' + state.activeTabs.connection.textContent);
    let resultContainer = document.createElement('div');
    resultContainer.className = 'result-record-container';

    let resultTitle = document.createElement('div');
    resultTitle.innerHTML = record.dn;
    resultTitle.className = 'result-title';
    resultTitle.onclick = function() {
        getFullResultSet(record.dn);
    }

    resultContainer.appendChild(resultTitle);
    parent.appendChild(resultContainer);

    if(isSingle) {
        resultTitle.click();
    }
}

function createSearchRulesDataset(searchRule) {
    let parent = document.querySelector('#search-datalist');

    let newSearchRuleElement = document.createElement('option');
    newSearchRuleElement.innerHTML = searchRule.name;
    
    parent.appendChild(newSearchRuleElement);
}

function createAuthProfilesDataset(authProfile) {
    let parent = document.querySelector('#profile-datalist');

    let newProfileElement = document.createElement('option');
    newProfileElement.innerHTML = authProfile.name;
    
    parent.appendChild(newProfileElement);
}