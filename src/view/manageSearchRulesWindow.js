let ManageSearchRulesWindow = {
    init: function() {
        document.querySelector('#search-rule-prompt-header-btn').onclick = function() {
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#search-rule-prompt-container').classList.toggle('hide');
        }

        document.querySelector('#search-rule-prompt-next-btn').onclick = function() {
            let validParams = true;
            let params = {};

            params.name = document.querySelector('#search-rule-prompt-rule-name').value;
            params.attributes = [];

            let attrArray = document.querySelector('#search-rule-prompt-attributes').value.split(',');

            for(let i = 0; i < attrArray.length; i++) {
                params.attributes.push(attrArray[i].trim());
            }

            for(var i in params) {
                if(params[i].length == 0) {
                    validParams = false;
                    console.log("Bad params. Warn user");
                    break;
                }
            }

            if(validParams) {
                document.querySelector('#search-rule-prompt-container').classList.toggle('hide');
                document.querySelector('#overlay').classList.toggle('hide');

                saveSearchRuleToConfig(params);
            }
        }
    }
}

editSearchRuleParameters = function(searchRuleObject) {
    if(typeof(searchRuleObject) !== "undefined") {
        document.querySelector('#search-rule-prompt-rule-name').value = (typeof(searchRuleObject.name) !== "undefined") ? searchRuleObject.name : "";

        let attributes = '';
        for(let i = 0; i < searchRuleObject.attributes.length; i++) {
            if(i>0) {
                attributes += ',' + searchRuleObject.attributes[i];
            } else {
                attributes += searchRuleObject.attributes[i];
            }
        }
        document.querySelector('#search-rule-prompt-attributes').value = (typeof(searchRuleObject.attributes) !== "undefined") ? attributes : "";


        onEnter = function() {
            document.querySelector('#search-rule-prompt-next-btn').click();
            onEnter = function() {};
        }
    
        document.querySelector('#overlay').classList.toggle('hide');
        document.querySelector('#search-rule-prompt-container').classList.toggle('hide');
    } else {
        console.log("passed in an undefined search rule object");
    }
}