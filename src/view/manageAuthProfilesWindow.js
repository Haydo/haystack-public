let ManageAuthProfilesWindow = {
    init: function() {
        document.querySelector('#auth-profile-prompt-header-btn').onclick = function() {
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#auth-profile-prompt-container').classList.toggle('hide');
        }

        document.querySelector('#auth-profile-prompt-next-btn').onclick = function() {
            let validParams = true;
            let params = {};

            params.name = document.querySelector('#auth-profile-prompt-profile-name').value;
            params.password = document.querySelector('#auth-profile-prompt-password').value;
            params.service = 'haystack';

            for(var i in params) {
                if(params[i].length == 0) {
                    validParams = false;
                    console.log("Bad params. Warn user");
                    break;
                }
            }

            if(validParams) {
                document.querySelector('#auth-profile-prompt-container').classList.toggle('hide');
                document.querySelector('#overlay').classList.toggle('hide');
                document.querySelector('#auth-profile-prompt-password').value = '';

                RendererDispatch.createAuthProfile(params);
                delete params.password;
                saveProfileToConfig(params);
            }
        }
    }
}


editAuthProfileParameters = function(authProfileOjbect) {
    if(typeof(authProfileOjbect) !== "undefined") {
        document.querySelector('#auth-profile-prompt-profile-name').value = (typeof(authProfileOjbect.name) !== "undefined") ? authProfileOjbect.name : "";

        onEnter = function() {
            document.querySelector('#auth-profile-prompt-next-btn').click();
        }
    
        document.querySelector('#overlay').classList.toggle('hide');
        document.querySelector('#auth-profile-prompt-container').classList.toggle('hide');
    } else {
        console.log("passed in an undefined auth profile object");
    }
}