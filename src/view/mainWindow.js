let isFilter;
let connections = {};
let searchRules = {};
let authProfiles = {};
let state = {
    activeTabs: {},
    activeResults: "",
    lastTabs: {},
    activeRecord: {}
}
/*
{
    activeTabs: {
        connection: "",
        record: {
            conn1: "",
            conn2: "",
        }
    },
    activeResults: "",
    lastTabs: {
        connection: "",
        record: {
            conn1: "",
            conn2: "",
        }
    },
    activeRecord: ""
}
*/
window.onload = function() {
    ManageConnectionWindow.init();
    ManageCredentialsWindow.init();
    ManagePasswordPromptWindow.init();
    ManageAuthProfilesWindow.init();
    ManageSearchRulesWindow.init();
    Events.init();
}

let getConnection = function(key) {
    if(connections.hasOwnProperty(key)) {
        return connections[key];
    } else {
        console.error('Connections object does not have connection:', key);
        return undefined;
    }
    
}

let getSearchRule = function(key) {
    if(searchRules.hasOwnProperty(key)) {
        return searchRules[key];
    } else {
        console.error('Search Rules object does not have search rule:', key);
        return undefined;
    }
}

let getauthProfile = function(key) {
    if(authProfiles.hasOwnProperty(key)) {
        return authProfiles[key];
    } else {
        console.error('Auth Profile object does not have auth profile:', key);
        return undefined;
    }
}

let getAllConnections = function() {
    return connections;
}

let getAllSearchRules = function() {
    return searchRules;
}

let getAllAuthProfiles = function() {
    return authProfiles;
}

let initConnectionList = function(config) {
    document.querySelector("#saved-connections-list").innerHTML = "";
    for(var i = 0; i < config.length; i++) {
        connections[config[i].name] = config[i];
        createConnectionObject(config[i]);
    }
}

let initSearchRules = function(config) {
    document.querySelector("#search-datalist").innerHTML = "";
    document.querySelector("#saved-search-rules-list").innerHTML = "";
    for(var i = 0; i < config.length; i++) {
        searchRules[config[i].name] = config[i];
        createSearchRulesDataset(config[i]);
        createSearchRuleObject(config[i]);
    }
}

let initAuthProfiles = function(config) {
    document.querySelector("#profile-datalist").innerHTML = "";
    document.querySelector("#saved-auth-profiles-list").innerHTML = "";
    for(var i = 0; i < config.length; i++) {
        authProfiles[config[i].name] = config[i];
        createAuthProfilesDataset(config[i]);
        createAuthProfileObject(config[i]);
    }
}

let selectConnectionTab = function(tab) {
    if(typeof(state.activeTabs.connection) === 'undefined' || !state.activeTabs.connection) {
        //set new tab to active
        state.activeTabs.connection = tab;
        //select new tab
        state.activeTabs.connection.classList.toggle('selected');
        return;
    }
    if(state.activeTabs.connection.textContent !== tab.textContent) {
        //unselect old tab
        state.activeTabs.connection.classList.toggle('selected');
        state.lastTabs.connection = state.activeTabs.connection;
        //set new tab to active
        state.activeTabs.connection = tab;
        //select new tab
        state.activeTabs.connection.classList.toggle('selected');
    }
}

let removeConnectionTab = function(tab) {
    let parent = tab.parentNode;
    if(state.activeTabs.connection === tab) {
        console.log('del of active');
        state.activeTabs.connection = "";
        delete state.activeRecord[tab.textContent];
        if(typeof(state.lastTabs.connection) !== 'undefined') {
            if(document.body.contains(state.lastTabs.connection)) {
                state.lastTabs.connection.click();
            }
        }
    }
    
    parent.removeChild(tab);

    //check if connection tab container is now empty. If so, delete it.
    if(!document.querySelector("#connection-tab-container").hasChildNodes()) {
        parent.parentNode.removeChild(parent);
    }
    //check if object tab container is now empty. If so, hide it.
    /*
    if(!document.querySelector('.object-tab-container.' + state.activeTabs['connection'].textContent).hasChildNodes()) {
        parent.parentNode.removeChild(parent);
    }
    */
}

let selectRecordTab = function(tab, connectionName) {
    if(typeof(state.lastTabs.record) === 'undefined') {
        state.lastTabs.record = {};
    }
    //check that record object exists
    if(typeof(state.activeTabs.record) !== 'undefined') {
        //check if there are currently no selected tabs
        if(state.activeTabs.record[connectionName]) {
            //check if tab is already selected tab
            if(state.activeTabs.record[connectionName].textContent !== tab.textContent) {
                //unselect old tab
                state.activeTabs.record[connectionName].classList.toggle('selected');
                state.lastTabs.record[connectionName] = state.activeTabs.record[connectionName];
                //set new tab to active
                state.activeTabs.record[connectionName] = tab;
                //select new tab
                state.activeTabs.record[connectionName].classList.toggle('selected');
            }
        } else {
            state.activeTabs.record[connectionName] = tab;
            //select new tab
            state.activeTabs.record[connectionName].classList.toggle('selected');
        }
    } else {
        //set new tab to active
        state.activeTabs.record = {};
        state.activeTabs.record[connectionName] = tab;
        //select new tab
        state.activeTabs.record[connectionName].classList.toggle('selected');
    }
}

let removeRecordTab = function(tab, connectionName) {
    let parent = tab.parentNode;
    if(state.activeTabs.record[connectionName] === tab) {
        console.log('del of active');
        state.activeTabs.record[connectionName] = "";
        if(state.lastTabs.record[connectionName]) {
            if(document.body.contains(state.lastTabs.record[connectionName])) {
                state.lastTabs.record[connectionName].click();
            }
        }
    } 
    
    parent.removeChild(tab);

    //check if object tab container is now empty. If so, hide it.
    /*
    if(!document.querySelector('.object-tab-container.' + state.activeTabs['connection'].textContent).hasChildNodes()) {
        parent.parentNode.removeChild(parent);
    }
    */
}

let populateQueryResults = function(results) {
    if(!document.querySelector('.query-results-container.' + state.activeTabs.connection.textContent).hasChildNodes()) {
        createResultsCount(results.length);
    }

    let isSingleRecord = (results.length === 1);

    for(var i = 0; i < results.length; i++) {
        createResultRecord(results[i], isSingleRecord);
    }
}

let clearQueryResults = function() {
    document.querySelector('.query-results-container.' + document.querySelector('.connection.tab.selected').textContent).innerHTML = "";
}

let establishConnection = function(connectionObject) {
    console.log("Will establish a connection to:", connectionObject.host + ":" + connectionObject.port);
}

let verifyConnection = function(connectionObject) {
    console.log("Will verify a connection to:", connectionObject.host + ":" + connectionObject.port);
}

let closeConnection = function(connectionName) {
    RendererDispatch.closeConnection(connectionName);
}

let saveConnectionParams = function() {
    let connection = {};

    connection.name = document.querySelector('#add-connection-connection-name').value;
    connection.host = document.querySelector('#add-connection-hostname').value;
    connection.port = document.querySelector('#add-connection-port').value;
    connection.useTLS = document.querySelector('#use-ldap-yes').checked;
    connection.usesAuthProfile = document.querySelector('#use-profile-yes').checked;
    connection.username = document.querySelector('#add-credentials-username').value;
    if(connection.usesAuthProfile) {
        connection.authProfile = document.querySelector('#profile-dropdown').value;
    }

    //TODO error handle dupe names
    connections[connection.name] = connection;
    RendererDispatch.saveConnectionToConfig(connection);
}

let deleteConnection = function(connectionName) {
    delete connections[connectionName];
    RendererDispatch.deleteConnectionFromConfig(connectionName);
}

let saveProfileToConfig = function(profile) {
    authProfiles[profile.name] = profile;
    RendererDispatch.saveProfileToConfig(profile);
}

let deleteAuthProfile = function(profileName) {
    delete authProfiles[profileName];
    RendererDispatch.deleteProfileFromConfig(profileName);
}

let saveSearchRuleToConfig = function(rule) {
    searchRules[rule.name] = rule;
    RendererDispatch.saveSearchRuleToConfig(rule);
}

let deleteSearchRule = function(ruleName) {
    delete searchRules[ruleName];
    RendererDispatch.deleteSearchRuleFromConfig(ruleName);
}

let connectToHost = function(connectionName, secret) {
    let connection = connections[connectionName];
    if(connection.usesAuthProfile) {
        RendererDispatch.connectWithProfile({name: connection.name, profile: connection.authProfile});
    } else if(typeof(secret) === 'undefined'){
        state.pendingConnection = connectionName;
        onEnter = function() {
            document.querySelector('#password-prompt-next-btn').click();
            onEnter = function(){};
        }
        document.querySelector('#overlay').classList.toggle('hide');
        document.querySelector('#password-prompt-username').value = connection.username;
        document.querySelector('#password-prompt-container').classList.toggle('hide');
    } else {
        RendererDispatch.connectWithBasicAuth({name: connection.name, secret: secret});
        secret = null;
    }
}

let queryByAttrs = function(connectionName, key, value) {
    ipcRenderer.send('ldap:client:query:attrs', {name: connectionName, key: key, value: value});
}

let queryBySearchRule = function(connectionName, key, value) {
    let keys = searchRules[key].attributes;
    ipcRenderer.send('ldap:client:query:rule', {name: connectionName, keys: keys, value: value});
}

let queryByFilter = function(connectionName, filter) {
    ipcRenderer.send('ldap:client:query:filter', {name: connectionName, filter: filter});
}

let getFullResultSet = function(dn) {
    let connection = state.activeTabs.connection.textContent;
    RendererDispatch.getFullResultSet({connection: connection, dn: dn});
}