let ManagePasswordPromptWindow = {
    init: function() {
            document.querySelector('#password-prompt-header-btn').onclick = function() {
            delete state.pendingConnection;
            document.querySelector('#password-prompt-password').value = "";
            document.querySelector('#overlay').classList.toggle('hide');
            document.querySelector('#password-prompt-container').classList.toggle('hide');
        }

        document.querySelector('#password-prompt-next-btn').onclick = function() {
            //connect to ldap host by passing in connection name
            //TODO Seems hacky
            let connectionName = state.pendingConnection;
            connectToHost(connectionName, document.querySelector('#password-prompt-password').value);
            delete state.pendingConnection;
            document.querySelector('#password-prompt-password').value = "";
            document.querySelector('#password-prompt-container').classList.toggle('hide');
            document.querySelector('#overlay').classList.toggle('hide');
        }
    }
}