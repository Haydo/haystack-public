const {app, ipcMain} = require('electron');

let win;

init = function(window) {
    win = window;

    ipcMain.on('window:close', (e, message) => {
        app.quit();
    });

    ipcMain.on('window:maximize', (e, message) => {
        if(win.isMaximized()) {
            win.unmaximize();
        } else {
            win.maximize();
        }
    });

    ipcMain.on('window:minimize', (e, message) => {
        win.minimize();
    });

    ipcMain.on('config:connection:modify', (e, message) => {
        modifyConfig("connections", message);
    });

    ipcMain.on('config:connection:delete', (e, message) => {
        deleteFromConfig("connections", message);
    });

    ipcMain.on('config:profile:modify', (e, message) => {
        modifyConfig("authProfiles", message);
    });

    ipcMain.on('config:profile:delete', (e, message) => {
        deleteFromConfig("authProfiles", message);
    });

    ipcMain.on('config:searchrule:modify', (e, message) => {
        modifyConfig("searchRules", message);
    });

    ipcMain.on('config:searchrule:delete', (e, message) => {
        deleteFromConfig("searchRules", message);
    });

    ipcMain.on('auth:keytar:profile:store', (e, message) => {
        storeAuthProfile(message);
        message = null;
    })

    ipcMain.on('auth:keytar:profile:delete', (e, message) => {
        deleteAuthProfile(message);
    })

    ipcMain.on('ldap:client:connect:profile', (e, message) => {
        connectToHost(message, true);
        message = null;
    });

    ipcMain.on('ldap:client:connect:basic', (e, message) => {
        connectToHost(message, false);
        message = null;
    });

    ipcMain.on('ldap:client:query:attrs', (e, message) => {
        queryByAttrs(message);
    });

    ipcMain.on('ldap:client:query:rule', (e, message) => {
        queryBySearchRule(message);
    });


    ipcMain.on('ldap:client:query:filter', (e, message) => {
        queryByFilter(message);
    });

    ipcMain.on('ldap:client:attr:replace', (e, message) => {
        replaceAttributeValue(message)
    });

    ipcMain.on('ldap:client:unbind', (e, message) => {
        unbind(message);
    });

    ipcMain.on('ldap:client:unbindAll', (e) => {
        unbindAll();
    });

    ipcMain.on('ldap:client:get:record', (e, message) => {
        getFullResultSet(message);
    });

    win.webContents.on('did-finish-load', () => {
        let config = getConfig();
        win.webContents.send("load:connection-list", config.connections);
        win.webContents.send("load:search-rules-list", config.searchRules);
        win.webContents.send("load:auth-profiles-list", config.authProfiles);
        win.webContents.openDevTools();
    })
}

exports.returnQueryResults = function(results) {
    win.webContents.send('ldap:client:query:results', results);
}

exports.returnResultSet = function(results) {
    win.webContents.send('ldap:client:object:results', results);
}

exports.returnSuccessfulConnection = function(connectionName) {
    win.webContents.send('ldap:client:connection:success', connectionName);
}

exports.init = init;