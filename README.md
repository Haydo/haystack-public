### Haystack

<img src="https://haydenives.com/res/haystack/main.jpg" alt="haystack UI" width="700"/>

Haystack is a relatively lightweight LDAP browser. In extreme alpha so please don't use on production directories!

This app is built on Electron and should be cross-platform, but has only been built/tested on Windows.

Additional configuration in package.json will be needed to build on other platforms.

### Installation

Building the setup executable requires [Yarn](https://yarnpkg.com/).

From the root directory run:
```sh
npm install
yarn compile
yarn dist
```

This should generate a folder called "dist" that contains your Setup.

### Running the App
Configuration files such as saved connections will be in your OS's "appdata" specific directory.

On Windows this is:
```
%appdata%\haystack
or
C:\Users\{UserName}\AppData\Roaming\haystack
```

Credentials will be stored in your OS specific credential manager. 
