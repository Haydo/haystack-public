/*
Notes:
Connections timing out
Need Create Search Rules Functionality
Need Create Profile Functionality
Need to implement keytar
Need to streamline connection calls (keep secure)
*/

const url = require("url");
const path = require("path");
const fs = require('fs');

const electron = require("electron");
const {app, BrowserWindow, Menu} = electron;

const mainDispatch = require('./src/node/mainDispatch.js');

//const electronVib = require("electron-vibrancy");
const keytar = require('keytar');
const ldap = require('ldapjs');

let win;
let config;
let connections = {};

const SERVICE_NAME = 'haystack';
let CONFIG_PATH = app.getPath('appData') + "/haystack/.config.json";


process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

app.on('ready', () => {
    win = createWindow();
    config = parseConfig();
    mainDispatch.init(win);

    win.once('ready-to-show', () => {
        win.show();
    });

    win.on('close', () => {
        app.quit();
    });
})

getConfig = function() {
    return config;
}

createWindow = function() {
    const width = electron.screen.getPrimaryDisplay().workAreaSize.width * 0.8;
    const height = electron.screen.getPrimaryDisplay().workAreaSize.height * 0.8;
    mainWindow = new BrowserWindow({
        width: width,
        height: height,
        //transparent: true,
        frame: false,
        webPreferences: {
            experimentalFeatures: true
        },
        //show: false,
        icon: __dirname + '\\res\\icons\\haystack-icon-v1.ico'
    });
    //mainWindow = new BrowserWindow({ width, height, transparent: true, frame: false });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, "/src/view/mainWindow.html"),
        protocol: "file:",
        slashes: true
    }));
    mainWindow.on('ready-to-show',function() { 

        //var electronVibrancy = require('..');
        //electronVib.SetVibrancy(mainWindow, 1);
    });

    mainWindow.on('maximize', () => {
        console.log("maxed!");
    });

    mainWindow.on('unmaximize', () => {
        console.log("unmaxed!");
    });

    mainWindow.on('minimize', () => {
        console.log("minned!");
    });

    mainWindow.on('restore', () => {
        console.log("restored!");
    });

    return mainWindow;
}

connectToHost = async function(connectionData, usingProfile) {


    let connection;
    let dataMatch = false;

    if(typeof config.connections !== 'undefined') {
        for(var i = 0; i < config.connections.length; i++) {
            if(config.connections[i].name == connectionData.name) {
                connection = config.connections[i];
                dataMatch = true;
                break;
            }
        }
        if(dataMatch) {
            let url = ((connection.useTLS) ? "ldaps://" : "ldap://") + connection.host + ":" + connection.port;
            let username = connection.username;
            let pass;
            let client = ldap.createClient({
                url: url
            });

            if(usingProfile) {
                for(var i = 0; i < config.authProfiles.length; i++) {
                    if(config.authProfiles[i].name == connectionData.profile) {
                        break;
                    }
                }
                pass = await keytar.getPassword(SERVICE_NAME, connectionData.profile);
            } else {
                pass = connectionData.secret;
                delete connectionData.secret;
            }

            client.bind(username, pass, (err) => {
                if(err) {
                    pass = null;
                    console.log(err);
                } else {
                    pass = null;
                    console.log('good');
                    connections[connectionData.name] = client;
                    mainDispatch.returnSuccessfulConnection(connectionData.name);
                }
            });

        } else {
            console.log('could not find config for connection named:', connectionData.name);
        }
    }
}

queryByAttrs = function(object) {
    if(!connections.hasOwnProperty(object.name)) {
        console.log('Can\'t find connection:', object.name);
        return;
    }
    let client = connections[object.name];
    let results = [];
    let key = object.key;
    let value = object.value;
    let filter = '(' + key + '=' + value + ')';
    console.log('filter:', filter)
    let opts = {
        filter: filter,
        scope: 'sub',
        attributes: ['dn']
    };

    client.search('', opts, (err, res) => {
        if(err) {
            console.log(err);
        } else {
            res.on('searchEntry', function(entry) {
                results.push(entry.object);
            });
            res.on('searchReference', function(referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function(err) {
                console.error('error: ' + err.message);
            });
            res.on('end', function(result) {
                console.log('status: ' + result.status);
                mainDispatch.returnQueryResults(results);
            });
        }
    });
}

queryBySearchRule = function(object) {
    if(!connections.hasOwnProperty(object.name)) {
        console.log('Can\'t find connection:', object.name);
        return;
    }
    let client = connections[object.name];
    let results = [];

    let searchesLeft = object.keys.length;

    for(var i = 0; i < object.keys.length; i++) {
        let key = object.keys[i];
        let value = object.value;
        let filter = '(' + key + '=' + value + ')';
        console.log('filter:', filter)
        let opts = {
            filter: filter,
            scope: 'sub',
            attributes: ['dn']
        };
    
        client.search('', opts, (err, res) => {
            if(err) {
                console.log(err);
            } else {
                res.on('searchEntry', function(entry) {
                    if(!results.includes(entry.object)) {
                        results.push(entry.object);
                    }
                });
                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });
                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });
                res.on('end', function(result) {
                    console.log(key, 'status: ' + result.status);
                    searchesLeft--;
                    if(searchesLeft == 0) {
                        mainDispatch.returnQueryResults(results);
                    }
                });
            }
        });
    }
}

queryByFilter = function(object) {
    if(!connections.hasOwnProperty(object.name)) {
        console.log('Can\'t find connection:', object.name);
        return;
    }
    let client = connections[object.name];
    let results = [];
    let opts = {
        filter: object.filter,
        scope: 'sub',
        attributes: ['dn']
    };

    client.search('', opts, (err, res) => {
        if(err) {
            console.log(err);
        } else {
            res.on('searchEntry', function(entry) {
                results.push(entry.object);
            });
            res.on('searchReference', function(referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function(err) {
                console.error('error: ' + err.message);
            });
            res.on('end', function(result) {
                console.log('status: ' + result.status);
                mainDispatch.returnQueryResults(results);
            });
        }
    });
}

getFullResultSet = function(data) {
    let connectionName = data.connection;
    let dn = data.dn;

    if(!connections.hasOwnProperty(connectionName)) {
        console.log('Can\'t find connection:', connectionName);
        return;
    }

    let client = connections[connectionName];

    let opts = {
        filter: '(cn=*)',
        scope: 'sub'
    };

    client.search(dn, opts, (err, res) => {
        if(err) {
            console.log(err);
        } else {
            res.on('searchEntry', function(entry) {
                results = entry.object;
            });
            res.on('searchReference', function(referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function(err) {
                console.error('error: ' + err.message);
            });
            res.on('end', function(result) {
                console.log('status: ' + result.status);
                mainDispatch.returnResultSet(results);
            });
        }
    });
}

replaceAttributeValue = function(modification) {
    let connectionName = modification.connection;
    let dn = modification.dn;
    let oldValue = modification.oldValue;
    let newValue = modification.newValue;
    let attribute = modification.attribute;

    let client = connections[connectionName];

    let deleteChange = {};
    deleteChange.operation = 'delete';
    deleteChange.modification = {};
    deleteChange.modification[attribute] = oldValue;

    let addChange = {};
    addChange.operation = 'add';
    addChange.modification = {};
    addChange.modification[attribute] = newValue;
    
    var deleteAttribute = new ldap.Change(deleteChange);

    var addAttribute = new ldap.Change(addChange);

    client.modify(dn, deleteAttribute, (err) => {
        if(err) {
            console.log(err);
        }
    });

    client.modify(dn, addAttribute, (err) => {
        if(err) {
            console.log(err);
        }
    });     
}

unbind = function(connectionName) {
    console.log('unbinding connection to:', connectionName);
    if(typeof(connections[connectionName]) !== 'undefined') {
        connections[connectionName].unbind((err) => {
            if(err) {
                console.log(err);
                console.log("Error unbinding connection:", i);
            }
        });
        delete connections[connectionName];
    } else {
        console.log('connection:', connectionName, 'does not seem to exist');
    }
    
}

unbindAll = function() {
    console.log('unbinding', Object.keys(connections).length, 'connections');
    for(var i in connections) {
        unbind(i);
    }
}

parseConfig = function() {
    let content;

    if(fs.existsSync(CONFIG_PATH)) {
        content = fs.readFileSync(CONFIG_PATH, "utf-8")
    } else {
        console.log("Creating config file");
        let defaultJSON = JSON.stringify({"connections": [],"authProfiles": [],"searchRules": []});
        fs.writeFileSync(CONFIG_PATH, defaultJSON);
        content = defaultJSON;
    }

    content = JSON.parse(content);
    return content;
}

modifyConfig = function(type, data) {
    let dataMatch = false;
    if(typeof config[type] !== 'undefined') {
        for(var i = 0; i < config[type].length; i++) {
            if(config[type][i].name == data.name) {
                config[type][i] = data;
                dataMatch = true;
                break;
            }
        }
        if(!dataMatch) {
            config[type].push(data);
        }

        fs.writeFileSync(CONFIG_PATH, JSON.stringify(config, undefined, 2));
        win.webContents.send("load:connection-list", config.connections);
        win.webContents.send("load:auth-profiles-list", config.authProfiles);
        win.webContents.send("load:search-rules-list", config.searchRules);

    } else {
        console.log('unsupported config type:', type);
    }
}

//data is just name for delete
deleteFromConfig = function(type, data) {
    let dataMatch = false;
    if(typeof config[type] !== 'undefined') {
        for(var i = 0; i < config[type].length; i++) {
            if(config[type][i].name == data) {
                config[type].splice(i, 1);
                dataMatch = true;
                break;
            }
        }

        fs.writeFileSync(CONFIG_PATH, JSON.stringify(config, undefined, 2));
        win.webContents.send("load:connection-list", config.connections);
        win.webContents.send("load:auth-profiles-list", config.authProfiles);
        win.webContents.send("load:search-rules-list", config.searchRules);

    } else {
        console.log('unsupported config type:', type);
    }
}

storeAuthProfile = function(profile) {
    keytar.setPassword(SERVICE_NAME, profile.name, profile.password);
    profile = null;
}

deleteAuthProfile = function(profileName) {
    keytar.setPassword(SERVICE_NAME, profile.name);
}
